let config = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.APP_PORT || 8080,
  kubernetes: {
    protocol: null,
    host: null,
    port: null
  }
}

config = Object.assign({}, require(`./${config.env}`))

module.exports = config
