const express = require('express')
const k8s = require('k8s')
const config = require('config')

const app = express()

app.get('/', function (req, res) {
  k8s.getServiceList((err, services) => {
    if (err) {
      res.end(`{"message":"${err.message}"}`)
    } else {
      res.end(JSON.stringify(services))
    }
  })
})

app.listen(config.port)
console.log(`listening at port ${config.port}`)
console.log(JSON.stringify(config, null, 2))
