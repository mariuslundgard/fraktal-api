const request = require('superagent')
const config = require('config')

function getServiceList (cb) {
  request.get(`${config.kubernetes.protocol}://${config.kubernetes.host}:${config.kubernetes.port}/api/v1/services`)
    .end((err, result) => {
      if (err) {
        cb(err, null)
      } else {
        cb(null, {
          _embedded: {
            app: result.body.items.filter((item) => {
              return item.metadata.namespace === 'default'
                && item.spec.ports.length
                && item.spec.ports[0].nodePort
            }).map((item) => {
              const name = item.metadata.name
              const href = `${config.kubernetes.protocol}://${config.kubernetes.host}:${item.spec.ports[0].nodePort}/`
              const links = {
                self: {
                  href
                }
              }
              const labels = item.metadata.labels
              const selector = item.spec.selector
              const createdAt = new Date(Date.parse(item.metadata.creationTimestamp))

              return {name, labels, selector, createdAt, _links: links}
            })
          }
        })
      }
    })
}

module.exports = {
  getServiceList
}
