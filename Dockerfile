FROM node:argon

MAINTAINER Stian Lund Johansen <stian.lund.johansen@nrk.no>
MAINTAINER Marius Lundgård <marius.lundgard@nrk.no>

COPY . /usr/src/app
WORKDIR /usr/src/app
RUN npm install --loglevel warn

EXPOSE 8080

CMD [ "npm", "start" ]
